FROM jenkins:2.46.3

USER root

RUN apt-get update && \
  apt-get install -y ruby && \
  rm -rf /var/lib/apt/lists/*
RUN gem install prawn prawn-svg && \
  gem install asciidoctor-pdf --pre

USER jenkins
